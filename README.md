## Special task - Sourcery for Developers

#### The task:

Implement a method that prints all dates between two given years that remain the same if numbers of the date are reversed.

Method signature:
*`static void printBonusDatesBetween(int fromYear, int toYear)`*

It should print dates in interval from fromYear (inclusive) to toYear (exclusive) that satisfy the condition.

For example, calling printBonusDatesBetween(2010, 2015) should print:
- 2010-01-02
- 2011-11-02

#### Solution:

The method accepts all numbers, and checks only 4 digit dates. The year value (int) is converted to full date ("XXXX-XX-XX") String representation using _StringBuilder_ and substring. The date is validated using the _LocalDate class_ from _Java Date Time API_.

#### How to run this program:

To run this program you will need to clone the project _[git clone https://gitlab.com/andrius.laurusevicius_sda/sourcerytask.git]_. Open this project in your IDE and run Main (Main.java) class.