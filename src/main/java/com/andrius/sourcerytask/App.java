package com.andrius.sourcerytask;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Code created by Andrius on 2020-09-22
 */
public class App {
    public static void main(String[] args) {

        printBonusDatesBetween(2010, 2015);
    }

    static void printBonusDatesBetween(int fromYear, int toYear) {
        for (int year = fromYear; year < toYear; year++) {
            if (String.valueOf(year).length() == 4) {
                String reversedYear = new StringBuilder(year + "").reverse().toString();
                try {
                    String date = String.format("%s-%s-%s", year, reversedYear.substring(0, 2), reversedYear.substring(2));
                    LocalDate.parse(date);
                    System.out.println(date);
                } catch (DateTimeParseException e) {
                    // No logger or System.err printing for clean console output
                }
            }
        }
    }
}
